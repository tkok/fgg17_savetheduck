﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Duck : MonoBehaviour {

	public FloatingObject floatingObject;

	public HingeJoint2D joint0;
	public DistanceJoint2D joint1;
	public FixedJoint2D joint2;

	public bool dead;
    public AudioClip[] eatenBySharkSounds;

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.gameObject.tag == "Shark") {
			//DEAD
			Dead();
			floatingObject.enabled = false;

            if(eatenBySharkSounds != null)
            {
                var src = GetComponent<AudioSource>();
                if (src == null)
                {
                    src = gameObject.AddComponent<AudioSource>();
                }
                src.volume = 0.2f;
				if (eatenBySharkSounds.Length != 0) {
					src.PlayOneShot(eatenBySharkSounds[Random.Range(0, eatenBySharkSounds.Length)]);
				}
            }
		} else if (collision.gameObject.tag == "killzone") {
			Destroy(transform.parent.gameObject);
		}
	}

	void Dead(){
		joint0.enabled = false;
		joint1.enabled = false;
		joint2.enabled = false;
		dead = true;
		Destroy(floatingObject);
		Destroy(this);
	}
}
