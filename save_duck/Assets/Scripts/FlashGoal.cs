﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashGoal : MonoBehaviour
{
    SpriteRenderer sr;
    public float flashFreq = 1.0f;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        var c = sr.color;
        c.a = Mathf.PingPong(Time.time * flashFreq, 1.0f);
        sr.color = c;
    }
}
