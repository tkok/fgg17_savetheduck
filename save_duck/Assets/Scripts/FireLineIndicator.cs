﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FireLineIndicator : MonoBehaviour {

	public LineRenderer line;
	public float value;
	public Vector3[] positions;
	public Transform flare;

	void Awake(){
	}
	
	// Update is called once per frame
	void Update () {
		UpdateLine(value);
	}

	public void UpdateLine(float percent){
		percent = Mathf.Clamp(Mathf.Clamp01(percent) * 0.9f, 0, 0.99f);
		int count = ((int)((positions.Length) * percent)) + 1;
		//Debug.Log("Count: " + count);
		float oneSegmentLenght = 1f / positions.Length;

		line.numPositions = count + 1;
		if (percent == 0) {
			line.numPositions = 0;
			return;
		}

		float percentsToLast = percent + oneSegmentLenght;
		for (int i = 0; i < count; ++i) {
			line.SetPosition(i, positions[i]);
			percentsToLast -= oneSegmentLenght;
		}

		//Debug.Log("Last percent: " + percentsToLast.ToString("0 %"));

		Vector3 lastPos =Vector3.Lerp(positions[count - 1], positions[count], (percentsToLast / oneSegmentLenght));
		line.SetPosition(count, lastPos);
		flare.localPosition = transform.localPosition + transform.localScale.x * lastPos;
	}
}
