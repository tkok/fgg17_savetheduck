﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waver : MonoBehaviour
{
    public int segmentCount = 250;
	public float segmentW = 0.01f;
	public WaveSegment[] segments;
	public WaveSegment[] segmentsTemp;
	public LineRenderer line;
	[Header("BOOM")]
	public int hitPosition = 500;
	public int hitW = 20;
	public AnimationCurve boomCurve;
	public float boomAmplitude;
	[Header("Stepping")]
	public int stepsPerFrame = 5;
	public float[] blur = new float[]{0.1f,0.1f,0.6f,0.1f,0.1f};
	[Header("idle aalto")]
	public IdleWave[] idleWaves;

	public float[] waterLevelOutPut;
	public float baseLevel;

	public bool autoStep;
	private Transform transformCache;

	public WaveMesh mesh;

	void InitSegments(){
		segments = new WaveSegment[segmentCount];
		segmentsTemp = new WaveSegment[segmentCount];
		line.numPositions = segmentCount;
		waterLevelOutPut = new float[segmentCount];
	}

	// Use this for initialization
	void Awake () {
		InitSegments ();
		transformCache = transform;
		mesh.width = segmentCount * segmentW;
		mesh.topVerticeCount = segmentCount;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (autoStep) {
			for (int i = 0; i < stepsPerFrame; ++i) 
			{
				Step();
			}
		}
		UpdateLine();
	}

	public void Step(){
		System.Array.Copy(segments, segmentsTemp, segmentCount);
		//Move waves
		for (int i = 1; i < segmentCount - 1; ++i) {
			segments[i].amplitudeLeft = segmentsTemp[i + 1].amplitudeLeft;
			segments[i].amplitudeRight = segmentsTemp[i - 1].amplitudeRight;
		}
		System.Array.Copy(segments, segmentsTemp, segmentCount);
		//Blur
		int windowWidthHalf = (blur.Length-1) / 2; //5 => 2
		int windowWidt = blur.Length;
		for (int i = windowWidthHalf; i < segmentCount - windowWidthHalf; ++i) {
			segments[i].amplitudeLeft = 0;
			segments[i].amplitudeRight = 0;
			for (int x = i - windowWidthHalf, index = 0; x < i + windowWidthHalf + 1; ++x, ++index) {
				segments[i].amplitudeLeft += segmentsTemp[x].amplitudeLeft * blur[index];
				segments[i].amplitudeRight +=  segmentsTemp[x].amplitudeRight * blur[index];
			}
		}
	}

	void UpdateLine(){
		for (int i = 0; i < segmentCount; ++i) {
			float a = segments[i].amplitudeLeft + segments[i].amplitudeRight;
			for (int x = 0; x < idleWaves.Length; ++x) {
				a += GetAmplitude(Time.time, i, idleWaves[x]);
            }
            waterLevelOutPut[i] = a;
            WaveMesh.singleton.AddPoint(new Vector3(segmentW * i, a));
            line.SetPosition(i, new Vector3(segmentW * i, a));
		}
        
        baseLevel = transform.position.y;
        WaveMesh.singleton.Apply();
    }


    static float GetAmplitude(float time, float x, IdleWave wave){
		return wave.amplitude * Mathf.Sin(time * wave.speed + wave.phaseAngle + x * wave.width);
	}

	public void Boom(){
		Boom(this.hitPosition, hitW, boomAmplitude);
	}

	public void Boom(float posX, float width, float amplitude){
		float realPosX = posX;
		posX = posX - this.transformCache.position.x;
		posX /= segmentW * transformCache.localScale.x;
		int posXi = Mathf.RoundToInt(posX);
		int widthI = Mathf.RoundToInt(width / (segmentW * transformCache.localScale.x));

		int startIndex = Mathf.Max(posXi - widthI, 0);
		int endIndex = Mathf.Min(segmentCount, posXi + widthI);
		for (int i = startIndex; i < endIndex; ++i) {
			float t = (i - startIndex) / (float)(endIndex - startIndex);
			segments[i].amplitudeLeft += amplitude * boomCurve.Evaluate (t);
			segments[i].amplitudeRight += amplitude * boomCurve.Evaluate (1-t);
		}
		UpdateLine();

        float nothing = 0;
		Splasher.Splash(new Vector3(realPosX, GetWaterLevelAt(realPosX, out nothing), 0), amplitude);
	}

	public float GetWaterLevelAt(float x, out float tangent)
	{
		x = x - this.transformCache.position.x;
		x /= segmentW * transformCache.localScale.x;
		int i = Mathf.RoundToInt(x);
		if (i < 1 || i > waterLevelOutPut.Length - 2) {
			tangent = 0;
			return -999f;
		}
		tangent = (waterLevelOutPut[i + 1] - waterLevelOutPut[i - 1]) / (segmentW * 2);
		return waterLevelOutPut[i] + baseLevel;
	}

	public float GetTangent(int i){
		if (i < 1 || i > waterLevelOutPut.Length - 2){
			return 0;
		}
		return (waterLevelOutPut[i + 1] - waterLevelOutPut[i - 1]) / (segmentW * 2);
	}

	public void ResetWater(){
		for (int i = 0; i < segmentCount; ++i) {
			segments[i] = new WaveSegment();
		}
		UpdateLine();
	}
}

public struct WaveSegment
{
	public float amplitudeLeft;
	public float amplitudeRight;
}

[System.Serializable]
public struct IdleWave{
	public float amplitude;
	public float speed;
	public float phaseAngle;
	public float width;
}