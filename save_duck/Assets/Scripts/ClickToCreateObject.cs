﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToCreateObject : MonoBehaviour {

	public GameObject prefabObject;
	public float followForce = 10;
	private Rigidbody2D currentObject;
	public float drag = 0.1f;

	public int startAmount = 6;
	public FrogCounter countter;

	public static ClickToCreateObject clicker;

	void Awake(){
		clicker = this;
		countter = FindObjectOfType<FrogCounter>();
		countter.Init(startAmount);
	}

	public void RemoveCurrentFrog(Frog frog){
		if (currentObject != null) {
			Frog findFrog = currentObject.GetComponentInParent<Frog>();
			if (findFrog == null) {
				Debug.LogError("No frog!!");
			}
			if (findFrog == frog) {
				currentObject.drag = 0;
				currentObject = null;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0) && currentObject == null) {
			if (countter.alive > 0) {
				GameObject newObject = Instantiate<GameObject>(prefabObject);
				Vector3 v3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				v3.z = transform.position.z;
				newObject.transform.position = v3;
				currentObject = newObject.GetComponentInChildren<FollowMouse>().GetComponent<Rigidbody2D>();
				currentObject.drag = this.drag;
				--countter.alive;
				countter.UpdateFrogs();
			}
		}
		if (currentObject != null) {
			if (Input.GetMouseButton(0)) {
				Vector2 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				mouse -= (Vector2)currentObject.transform.position;
				currentObject.AddForce(mouse * followForce);
			} else {
				currentObject.drag = 0;
				currentObject = null;
			}
		}
	}
}
