﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogCounter : MonoBehaviour {

	public GameObject[] frogs;
	public int alive;

	public void Init(int amount){
		alive = amount;
		UpdateFrogs();
	}

	public void UpdateFrogs(){
		for (int i = 0; i < frogs.Length; ++i) {
			frogs[i].SetActive(i < alive);
		}
	}
}
