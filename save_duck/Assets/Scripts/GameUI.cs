﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour {

	public void RestartGame(){
		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
	}

	public void ToMenu(){
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}

	private void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) {
			ToMenu();
		}
	}
}
