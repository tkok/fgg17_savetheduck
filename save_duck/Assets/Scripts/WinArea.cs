﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinArea : MonoBehaviour {
	public Collider2D colliderBox;


	void OnCollisionEnter2D(Collision2D collision){

		//Debug.Log("Collsion win box!!");
		if (collision.gameObject.tag == "Duck") {
			GameManager.manager.WinRound();
			this.colliderBox.enabled = false;

		}
	}
}
