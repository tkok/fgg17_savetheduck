﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameCanvas : MonoBehaviour 
{
	public void NewGame(){
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}
}
