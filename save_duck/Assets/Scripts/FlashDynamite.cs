﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashDynamite : MonoBehaviour
{
    SpriteRenderer sr;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        float a = Mathf.PerlinNoise(0, Time.time * 10.0f);
        sr.color = new Color(1, 1, 1, a);
    }
}
