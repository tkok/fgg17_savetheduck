﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotationStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.localEulerAngles = new Vector3(0, 0, 360 * Random.value);
	}
}
