﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TangentTester : MonoBehaviour {
	public Waver waver;
	public LineRenderer line;

	void Update(){
		line.numPositions = waver.segmentCount;
		for (int i = 0; i < waver.segmentCount; ++i) {
			line.SetPosition(i, new Vector3(waver.segmentW * i, waver.GetTangent(i))); 
		}
	}
}
