﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingObject : MonoBehaviour 
{
	private Rigidbody2D rb;
	private Transform transformCache;
	public Waver waver;
	public float waterDrag = 0.5f;
	public float waterForce = 5;
	public float tangentForce = 5;
	public float width = 10;
	public float waveCreationAmplitudeMultiplier = 0.2f;
	public float waterHitDamp = 0.5f;
	public int inWater = 0;
	public int dryTime = 5;

	public float extraWaterDelta = 0.1f;
	public float extraArea = 0.2f;

	//public SpriteRenderer spriteRenderer;
	//public Color inWaterColor;
	//public Color inAirColor;

	//public TextMesh debugText;

	void Start() {
		rb = GetComponent<Rigidbody2D>();
		waver = FindObjectOfType<Waver>();
		transformCache = transform;
	}

	void FixedUpdate()
	{
		if (!rb.simulated)
			return;
		float waterTangent = 0;
		float waterLevel = waver.GetWaterLevelAt(transformCache.position.x, out waterTangent);
		//Debug.DrawLine(transformCache.position, new Vector3(transformCache.position.x, waterLevel), Color.white);
		//debugText.text = "pos y: " + transformCache.position.y.ToString("0.00")
		//	+ "\nwaterl: "+waterLevel.ToString("0.00") 
		//	+ "\ntang: " + waterTangent.ToString("0.00");
		if (waterLevel > transformCache.position.y) {
			if (inWater <= 0) {
				waver.Boom(transformCache.position.x, width, rb.velocity.y * waveCreationAmplitudeMultiplier);
				rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * waterHitDamp);
			}
			inWater = dryTime;
			float waterDelta = waterLevel - transformCache.position.y;
			float extraForce = extraWaterDelta * Mathf.Clamp01((waterDelta / extraArea));
			//debugText.text += "\nExtra: " + extraForce.ToString("0.00");
			rb.AddForce(new Vector2(-waterTangent * tangentForce, (waterForce) * (waterDelta + extraForce)), ForceMode2D.Force);
			rb.drag = waterDrag;
		} else {
			inWater = Mathf.Max(inWater - 1, -1);
			rb.drag = 0.0f;
		}
		//spriteRenderer.color = inWater > 0 ? inWaterColor : inAirColor;
	}
}
