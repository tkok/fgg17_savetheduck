﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SepcialSelection : MonoBehaviour {

	public bool dynamite;
	public bool bowlingball;

	public SpriteRenderer[] stock;

	void OnCollisionEnter2D(Collision2D collision){
		if (collision.gameObject.tag == "Frog") {
			Frog frog = collision.gameObject.GetComponentInParent<Frog>();
			if (frog == null) {
				return;
			}
			bool ok = frog.ChangeFrogSpecials(this.dynamite, this.bowlingball);
			if (!ok) {
				return;	
			}
			for (int i = 0; i < stock.Length; ++i) {
				if (stock[i].enabled) {
					stock[i].enabled = false;
					if (i == stock.Length - 1) {
						gameObject.SetActive(false);
					}
					break;
				}
			}

		}
	}
}
