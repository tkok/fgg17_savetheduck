﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveMesh : MonoBehaviour
{
    Mesh mesh;
    public float width = 2;
    
    public int topVerticeCount = 10;
    int currentVertexIndex = 0;
    Vector3[] vertices;
    
    MeshFilter mf;

    public AnimationCurve topShape;
    public AnimationCurve bottomShape;

    public static WaveMesh singleton;

    void Start()
    {
        singleton = this;

        vertices = new Vector3[topVerticeCount * 3];
        int[] triangles = new int[vertices.Length * 4];
        Vector2[] uvs = new Vector2[vertices.Length];

        for (int i = 0; i < topVerticeCount; i++)
        {
            float x = i / (float)topVerticeCount;
            vertices[i]                       = new Vector3(x * width, Mathf.Sin(x * Mathf.PI * 2.0f) + 1.5f, 0.0f);
            vertices[i + topVerticeCount]     = new Vector3(x * width, Mathf.Sin(x * Mathf.PI * 2.0f) + 1.4f, 0.0f);
            vertices[i + topVerticeCount * 2] = new Vector3(x * width, bottomShape.Evaluate(x), 0.0f);

            uvs[i] = new Vector2(0.0f, 1.0f);
            uvs[i + topVerticeCount] = new Vector2(0.0f, 0.70f);
            uvs[i + topVerticeCount * 2] = new Vector2(0.0f, 0.0f);

            if (i + 1 == topVerticeCount)
                break;

            triangles[12 * i + 0] = i;
            triangles[12 * i + 1] = i + 1;
            triangles[12 * i + 2] = i + topVerticeCount + 1;
            triangles[12 * i + 3] = i;
            triangles[12 * i + 4] = i + topVerticeCount + 1;
            triangles[12 * i + 5] = i + topVerticeCount;
            triangles[12 * i + 6 + 0] = i + topVerticeCount;
            triangles[12 * i + 6 + 1] = i + topVerticeCount + 1;
            triangles[12 * i + 6 + 2] = i + topVerticeCount + topVerticeCount + 1;
            triangles[12 * i + 6 + 3] = i + topVerticeCount;
            triangles[12 * i + 6 + 4] = i + topVerticeCount + topVerticeCount + 1;
            triangles[12 * i + 6 + 5] = i + topVerticeCount + topVerticeCount;
        }

        mf = GetComponent<MeshFilter>();
        if(null == mf)
        {
            mf = gameObject.AddComponent<MeshFilter>();
        }
        
        mf.mesh.vertices = vertices;
        mf.mesh.triangles = triangles;
        mf.mesh.uv = uvs;


        if (null == GetComponent<MeshRenderer>())
        {
            gameObject.AddComponent<MeshRenderer>();
        }
    }
    
    public void AddPoint(Vector3 point)
    {
        Debug.Assert(currentVertexIndex < topVerticeCount);
        float t = point.x / width;
        
        float bottom = bottomShape.Evaluate(t);
        float top = topShape.Evaluate(t);
        float y = point.y * top;
        //point.y = Mathf.Min(Mathf.Max(y, bottom), top);
        point.y = Mathf.Max(y, bottom);
        vertices[currentVertexIndex] = point;
        //point.y = Mathf.Min(Mathf.Max(y - 0.05f, bottom), top);
        point.y = Mathf.Max(y - 0.05f, bottom);
        vertices[currentVertexIndex + topVerticeCount] = point;
        currentVertexIndex += 1;
    }

    public void Apply()
    {
        currentVertexIndex = 0;
        mf.mesh.vertices = vertices;
    }
	public bool debugStart;
    void Update()
    {
		if (debugStart) {
			debugStart = false;
			Start();
		}
    }
}
