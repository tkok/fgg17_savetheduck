﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvas : MonoBehaviour {

	public void Quit(){
		Application.Quit();
	}

	public void StartGame(){
		GameManager.manager.StartNewGame();
	}

	private void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Quit();
		}
	}
}
