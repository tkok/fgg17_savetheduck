﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : MonoBehaviour {

	private bool hasDynamite = false; //make explosions
	private bool hasBowlingBall = false; //Change mass

	public SpriteRenderer torso;

	public Sprite normalSprite;
	public Sprite dynamiteSprite;
	public Sprite bowlingBallSprite;

	public FloatingObject floating;

	public float bowlingWidth = 100;
	public float bowlingPower = 100;
	public float bowlingMass = 1;


	public Rigidbody2D torsoRB;
	public SpriteRenderer flare;

	[Header("Dynamite")]
	public FireLineIndicator dynamiteCountDown;
	public float dynamiteDuration = 5;
	private float dynamiteStartTime;
	public float dynamiteWidth = 0.3f;
	public float dynamitePower = 0.3f;
	public float dynamiteFloatingPower = 250;
	public float dynamiteRB2DExplosionMultiplier = 1;
    
	public Waver waver;
    
    public AudioClip bowlingBallClank;
    public AudioClip dynamiteIgnition;
    public AudioClip dynamiteExplosion;
    bool hasCollidedWithBottom = false;
    private AudioSource audioSource;
	private AudioSource swallowAudioSource;
    
    private bool hasBeenInWater = false;
	private bool alive = true;

	public AudioClip[] swallowSounds;

	void Boom2D(){
		Rigidbody2D[] rbs = FindObjectsOfType<Rigidbody2D>();
		Vector2 explosionPosition = torsoRB.position + new Vector2(0, -0.2f);
		for (int i = 0; i < rbs.Length; ++i) {
			Vector2 exp = explosionPosition - rbs[i].position;
			if (exp.sqrMagnitude == 0)
				continue;
			rbs[i].AddForce(exp * (dynamiteRB2DExplosionMultiplier / exp.magnitude), ForceMode2D.Impulse);
			//Debug.DrawLine(explosionPosition, rbs[i].position, Color.red, 1, false);
			//Debug.Log("Force: " + (exp * (dynamiteRB2DExplosionMultiplier / exp.magnitude)).ToString("0.00"));
		}

        audioSource.Stop();
        audioSource.PlayOneShot(dynamiteExplosion);
    }

	void Awake(){
		flare.enabled = false;
		dynamiteCountDown.value = 0;
		this.waver = FindObjectOfType<Waver>();

        audioSource = GetComponent<AudioSource>();
        if(null == audioSource)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }

		swallowAudioSource = GetComponent<AudioSource>();
		if(null == swallowAudioSource)
		{
			swallowAudioSource = gameObject.AddComponent<AudioSource>();
		}
    }

	void Update(){
		if (hasDynamite) {
			float timeDelta = Time.time - dynamiteStartTime;
			float timeLeft = dynamiteDuration - timeDelta;
			if (timeLeft < 0) {
				hasDynamite = false;
				dynamiteCountDown.value = 0;
				waver.Boom(torsoRB.position.x, dynamiteWidth, dynamitePower);
				Boom2D();
				Die();
			} else {
				dynamiteCountDown.value = timeLeft / dynamiteDuration;
			}
		}
		if (!hasBeenInWater && floating.inWater > 0) {
			hasBeenInWater = true;
			ClickToCreateObject.clicker.RemoveCurrentFrog(this);
		}
	}

	public bool ChangeFrogSpecials(bool dynamite, bool bowlingBall){
		
		if (hasBowlingBall || hasDynamite) {
			return false;
		}

		if (dynamite) {
            audioSource.volume = 0.1f;
            audioSource.PlayOneShot(dynamiteIgnition);
            hasDynamite = true;
			this.torso.sprite = dynamiteSprite;
			flare.enabled = true;
			floating.waterForce = dynamiteFloatingPower;
			dynamiteStartTime = Time.time;

		}
		if (bowlingBall) {
			hasBowlingBall = true;
			this.torso.sprite = bowlingBallSprite;
			floating.width = bowlingWidth;
			floating.waveCreationAmplitudeMultiplier = bowlingPower;
			torsoRB.mass = bowlingMass;
		}

		if (null != swallowSounds && swallowSounds.Length > 0) {
			swallowAudioSource.PlayOneShot(swallowSounds[Random.Range(0, swallowSounds.Length)]);
		}

		return true;
	}

	public void Collsion(Collision2D collision)
    {
		if(false == hasCollidedWithBottom && hasBowlingBall && collision.collider.gameObject.name == "tub_fore")
        {
            audioSource.volume = 0.5f;
            audioSource.PlayOneShot(bowlingBallClank);
            hasCollidedWithBottom = true;
        }
	}

	public void Die(){
		var joints = GetComponentsInChildren<HingeJoint2D>();
		for (int i = 0; i < joints.Length; ++i) {
			joints[i].enabled = false;
		}

		var joints2 = GetComponentsInChildren<DistanceJoint2D>();
		for (int i = 0; i < joints2.Length; ++i) {
			joints2[i].enabled = false;
		} 

		torso.sprite = null;
		flare.enabled = false;
		Destroy(floating);
		Destroy(this);
	}
}
