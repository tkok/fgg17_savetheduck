﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splasher : MonoBehaviour
{
    static Splasher singleton;

    public GameObject splashPrefab;
    public AudioClip[] splashAudioClips;
    AudioSource[] audioSources;
    int currentIndex = 0;

	public int maxEmit = 100;
	public float minForce = 0.01f;
	public float maxForce = 0.6f;

	public Vector2 minVelosity = new Vector2(10f, 90f / 3f);
	public Vector2 maxVelosity = new Vector2(20f, 90f * 3f);

	public List<ParticleSystem> splashes = new List<ParticleSystem>();

	public float killSystemAfterSeconds = 3;

    void Start()
    {
        singleton = this;

        audioSources = new AudioSource[6];
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i] = gameObject.AddComponent<AudioSource>();
        }
    }

    public static void Splash(Vector3 pos, float force)
    {
        singleton.SplashImpl(pos, force);
    }

    void SplashImpl(Vector3 pos, float force)
    {
		force = Mathf.Abs(force);
		float t = Mathf.Max(0, (force - minForce) / (maxForce - minForce));
		if (t == 0) {
			return;
		}
        var splash = MonoBehaviour.Instantiate<GameObject>(splashPrefab);
		ParticleSystem ps = splash.GetComponent<ParticleSystem>();
		ParticleSystem.MainModule module = ps.main;
		splashes.Add(ps);
		module.startSpeed = new ParticleSystem.MinMaxCurve(
			Mathf.Lerp(minVelosity.x, maxVelosity.x, t),
			Mathf.Lerp(minVelosity.y, maxVelosity.y, t));
		//ps.set.main = module;
		ps.Emit(Mathf.RoundToInt(maxEmit * t));
        splash.transform.position = pos;
		ps.Play(false);

		for (int i = 0; i < splashes.Count; ++i) {
			if (splashes[i] == null) {
				splashes.RemoveAt(i);
				--i;
			}
			else if(splashes[i].time > killSystemAfterSeconds || !splashes[i].isPlaying) {
				Destroy(splashes[i].gameObject);
				splashes.RemoveAt(i);
				--i;
			}
		}

        if (t < 0.2f)
            return;

		int splashAudioIndex = Mathf.Min(splashAudioClips.Length - 1, Mathf.FloorToInt(splashAudioClips.Length * t));

		if (splashAudioIndex < 0)
            return;

		AudioClip clip = splashAudioClips[splashAudioIndex];
        audioSources[currentIndex].volume = splashAudioIndex == 0 ? 0.05f :
                                            splashAudioIndex == 1 ? 1.0f : 0.5f;
        audioSources[currentIndex].PlayOneShot(clip);

        currentIndex = (currentIndex + 1) % audioSources.Length;

    }
}
