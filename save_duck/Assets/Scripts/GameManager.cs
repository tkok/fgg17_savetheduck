﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public Waver waver;
    public int level = 0;
    public static GameManager manager;

    bool gameWon;
    float gameWonTime;
    Vector3 cameraOrigin;
    public Vector3 cameraTarget;
    float cameraSizeOrigin;
    float cameraSizeTarget = 0.4f;
    public AudioClip winAudio;

    public void Awake()
    {
        if (manager != null && manager != this)
        {
            Destroy(this);
            return;
        }
        manager = this;
        DontDestroyOnLoad(gameObject);
    }

    public void StartNewGame()
    {
        level = 1;
        UnityEngine.SceneManagement.SceneManager.LoadScene(level);
    }

    public void WinRound()
    {
        if (false == gameWon)
        {
            gameWon = true;
            gameWonTime = Time.time;

            if(null != winAudio)
            {
                var asrc = GetComponent<AudioSource>();
                if (null == asrc)
                {
                    asrc = gameObject.AddComponent<AudioSource>();
                }

                asrc.volume = 0.25f;
                asrc.PlayOneShot(winAudio);

                cameraOrigin = Camera.main.transform.position;
                cameraSizeOrigin = Camera.main.orthographicSize;
            }
        }
    }

    void Update()
    {
        if (false == gameWon)
            return;

        float waitTime = 4.0f;
        float effectTime = waitTime * 0.33f;

        Camera.main.transform.position = Vector3.Lerp(cameraOrigin, cameraTarget, Mathf.Clamp01((Time.time - gameWonTime) / effectTime));
        Camera.main.orthographicSize = Mathf.Lerp(cameraSizeOrigin, cameraSizeTarget, Mathf.Clamp01((Time.time - gameWonTime) / effectTime));

        if (gameWonTime + waitTime < Time.time)
        {
            gameWon = false;


            //Load next level
            ++level;
            UnityEngine.SceneManagement.SceneManager.LoadScene(level);
        }
    }
}
