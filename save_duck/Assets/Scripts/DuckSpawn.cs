﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckSpawn : MonoBehaviour {

	public GameObject duckPrefab;
	public Duck currentDuck;
	public int spawnCount;

	public int spawnLimitOnMobile = 20;

    public AudioClip[] spawnSounds;
    AudioSource audioSource;


	// Update is called once per frame
	void Update () {
		if (this.currentDuck == null) {
			if (Application.platform == RuntimePlatform.IPhonePlayer && spawnLimitOnMobile <= spawnCount) {
				return;
			}
			currentDuck = Instantiate<GameObject>(duckPrefab).GetComponentInChildren<Duck>();
			currentDuck.transform.parent.transform.position = transform.position;
			spawnCount++;

            if (0 == UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex)
                return;

            if(audioSource == null)
            {
                audioSource = GetComponent<AudioSource>();
                if (audioSource == null)
                {
                    audioSource = gameObject.AddComponent<AudioSource>();
                }
            }

            if(audioSource != null && spawnSounds != null && spawnSounds.Length > 0)
            {
                audioSource.PlayOneShot(spawnSounds[Random.Range(0, spawnSounds.Length)]);
            }

        } else {
			if (currentDuck.dead) {
				this.currentDuck = null;
			}
		}
	}
}
