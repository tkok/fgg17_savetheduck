﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Waver))]
public class WaverEditor : Editor {

	public override void OnInspectorGUI()
	{
		if (GUILayout.Button ("Boom")) {
			(target as Waver).Boom ();
		}
		if (GUILayout.Button ("Step")) {
			(target as Waver).Step ();
		}
		base.OnInspectorGUI ();
	}

}
